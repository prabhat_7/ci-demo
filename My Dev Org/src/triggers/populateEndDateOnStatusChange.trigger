trigger populateEndDateOnStatusChange on Account (before insert,before update) {

    set<Id> accIds = new set<Id>();
    
    if(trigger.Isinsert){
        for(Account acc : trigger.new){
            if(acc.Status__c=='Planned'){
                populateEndDateonAccount.calculateEndDate(acc);
            }
        }
    }
    
    if(trigger.isUpdate){
        for(Account acc : trigger.new){
            if(acc.Status__c!=null && acc.Status__c=='Planned' && trigger.oldMap.get(acc.Id).Status__c!='Planned'){
                accIds.add(acc.Id);
            }
        }
    
    
        if(accIds!=null && accIds.size()>0){
            
            list<Contact> contactList = new list<Contact>([SELECT Id,AccountId,Start_Date__c FROM Contact WHERE AccountId IN: accIds ORDER BY Start_Date__c ASC]);
            map<Id,Date> accDateMap = new map<Id,Date>();
            map<Id,list<Contact>> conMaps = new map<Id,list<Contact>>();
            
            if(!contactList.isEmpty()){
                for(Contact con : contactList){
                    if(con.Start_Date__c!=null && con.AccountId!=null){
                        accDateMap.put(con.AccountId,con.Start_Date__c);
                        system.debug('dateMap--->'+accDateMap);
                    }
                    list<Contact> templist = new list<Contact>();
                    templist.add(con);
                    conMaps.put(con.AccountId,templist);
                    system.debug('conMap--->'+conMaps); 
                }
           }
           for(Account acc : trigger.new){
               if(conMaps.containskey(acc.Id)){
                   acc.End_Date__c = accDateMap.get(acc.Id);
               }
               else{
                   acc.End_Date__c = populateEndDateonAccount.calculateEndDate(acc);
                   system.debug('classDate--->'+acc.End_Date__c);
               }
           }
        }
    }
}