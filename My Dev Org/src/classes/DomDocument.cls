public class DomDocument {

    // Pass in the URL for the request
    // For the purposes of this sample,assume that the URL
    // returns the XML shown above in the response body
    public static void parseResponseDom(String url){

       string xml = '<OrderDetails>'+
                            '<OrderDetailID>2584</OrderDetailID>'+
                            '<GiftWrapCost>0.0000</GiftWrapCost>'+
                            '<GiftWrapNote><NewBie><Gbie>nnnnn</Gbie></NewBie></GiftWrapNote>'+
                            '<ProductCode>800006</ProductCode>'+ 
                           '</OrderDetails>';
       Dom.Document doc = new Dom.Document();
       doc.load(xml);

        //Retrieve the root element for this document.
        Dom.XMLNode ordDtls = doc.getRootElement();

            String OrderDetailId= ordDtls.getChildElement('OrderDetailID', null).getText();
            String prdCode = ordDtls.getChildElement('ProductCode', null).getText();
            String gfwrap = ordDtls.getChildElement('GiftWrapNote', null).getChildElement('NewBie', null).getChildElement('Gbie', null).getText();
            
            System.debug('OrderDtlID: ' + OrderDetailId);
            System.debug('Prod Code: ' + prdCode );
            System.debug('gfwrap: ' + gfwrap );
        // print out specific elements
        

        // Alternatively, loop through the child elements.
        // This prints out all the elements of the address
        
    }
}