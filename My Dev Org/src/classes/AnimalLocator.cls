public class AnimalLocator {

    public static String getAnimalNameById(Integer n){
        Object str;
        Http http = new Http();
        Httprequest req = new Httprequest();
        req.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/'+n);
        req.setMethod('GET');
        Httpresponse resp = http.send(req);
        if(resp.getStatusCode()==200){
           Map<String,Object> onjMap = (Map<String,Object>)JSON.deserializeUntyped(resp.getBody());
           map<String,Object> result = (Map<String,Object>)onjMap.get('animal'); 
           str = result.get('name');
        }
        return String.valueOf(str);
      }
}