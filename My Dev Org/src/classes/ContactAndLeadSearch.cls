public class ContactAndLeadSearch {
    
    public static List<List< SObject>> searchContactsAndLeads(String str){
        String names = str;
        List<List<SObject>> conLeads = [FIND :names IN ALL FIELDS RETURNING  Lead(Name,FirstName,LastName),Contact(FirstName,LastName)];
        
        return conLeads;
    }

}