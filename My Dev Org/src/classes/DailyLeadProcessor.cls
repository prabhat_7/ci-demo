global class DailyLeadProcessor implements schedulable{
    
    global void execute(SchedulableContext sc){
        
        List<Lead> allLeads = new list<Lead>([SELECT Id,LeadSource FROM Lead WHERE LeadSource =:null or LeadSource =:'']);
        
        for(Lead l : allLeads){
            l.LeadSource ='DreamForce';
        }
        update allLeads;
        
    }

}