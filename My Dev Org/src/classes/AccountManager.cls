@RestResource(urlMapping='/Accounts/*')
global with sharing class AccountManager {
     
     
     // My Jenkin Change 
    @HttpGet
    global static Account getAccount(){
		RestRequest request = RestContext.request;
        String orig = request.requestURI.removeEnd('/contacts');
        String accId = orig.substring(orig.lastIndexOf('/')+1);
        Account result = [SELECT Id,Name,(SELECT Id,Name FROM Contacts) FROM Account WHERE Id=:accId];        
        return result;
    }

	@HttpPost
	 global static Id createAccount(String Name){
		RestRequest request = RestContext.request;
        String orig = request.requestURI.removeEnd('/contacts');
        String accId = orig.substring(orig.lastIndexOf('/')+1);
       // Account result = [SELECT Id,Name,(SELECT Id,Name FROM Contacts) FROM Account WHERE Id=:accId];        
        Account acc = new Account(Name='Test Name');
        insert acc;
        return acc.id;
    }

}