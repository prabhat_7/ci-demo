public with sharing class RedirectCampController{

  String RecordTypeId;
  String ent;
  public String retUrl{get;set;}
  public Boolean showError{get;set;}
  public RedirectCampController(ApexPages.StandardController stdCnt){
    showError = false;
    retUrl = ApexPages.currentPage().getParameters().get('retURL');
    RecordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
    ent = ApexPages.currentPage().getParameters().get('ent');
  }
  public PageReference redirect(){
    PageReference pg;
    if(RecordTypeId == Schema.SObjectType.Campsite__c.getRecordTypeInfosByName().get('Campsites1').getRecordTypeId()){
      pg = new PageReference('/apex/ManageRedirection?retUrl='+retUrl+'&rec='+RecordTypeId);  
    }else if(RecordTypeId == Schema.SObjectType.Campsite__c.getRecordTypeInfosByName().get('Campesite2').getRecordTypeId()){
      pg = new PageReference('/apex/ManageRedirection?retUrl='+retUrl+'&rec='+RecordTypeId);  
    }
    system.debug('pg: '+pg);
    pg.setRedirect(true);
    return pg;
  }
}