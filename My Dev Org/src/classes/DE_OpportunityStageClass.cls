public with sharing class DE_OpportunityStageClass {

    private final Opportunity mysObject;
    public String oppStageList {get;set;}
    public String oppStageNum {get;set;}
    
    
    Opportunity opp;
    Map<string,string> oppStageMapWithRT = new Map<string,string>();
    
    public DE_OpportunityStageClass(ApexPages.StandardController stdController) {
        this.mysObject = (Opportunity)stdController.getRecord();
        //opp = [select recordType.Name from Opportunity where id =: mysObject.Id];
        oppStageList = '[\'';
        oppStageNum = '[\'';
        getPicklistValues();
    }

    public void getPicklistValues()
    {
        list<string> strList = new list<string>();
        list<String> tempList = label.oppstages.split(';');
        for(String str: tempList)
        {
            system.debug('str == '+str);
            strList.clear();
            strList.add(str);
            system.debug('strList size == '+strList.size());
            system.debug('strList == '+strList);
            //oppStageMapWithRT.put(strList[0],strList[1]);
            //system.debug('oppStageMapWithRT == '+oppStageMapWithRT);
        }
        
        //Set Stage Number 
        Integer i = 1;
        //Query for All Stages in list that aren't 0% Probable (aka 'Closed Lost')
        for (OpportunityStage os : [select MasterLabel, SortOrder from OpportunityStage 
            where IsActive=true order by SortOrder])
        {
                oppStageList += os.MasterLabel + '\', \'';
                oppStageNum += i + '\', \'';
                i += 1;
           
        }

        oppStageList = oppStageList.substring(0, oppStageList.length()-3);
        oppStageList = oppStageList + ']';

        oppStageNum = oppStageNum.substring(0, oppStageNum.length()-3);
        oppStageNum = oppStageNum + ']';
    }
}