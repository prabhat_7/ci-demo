public class ManageRedirectionController {

    public Campsite__c camp {get;set;}
    public Boolean showFirst {get;set;}
    public Boolean showSecond {get;set;}
    String recType ;
    public Date testDate{get;set;}
    public ManageRedirectionController(){
    
        camp = new Campsite__c();
        showFirst = false;
        showSecond = false;
        Id RecType1 = Schema.SObjectType.Campsite__c.getRecordTypeInfosByName().get('Campsites1').getRecordTypeId();
        Id RecType2 = Schema.SObjectType.Campsite__c.getRecordTypeInfosByName().get('Campesite2').getRecordTypeId();
        recType = ApexPages.currentPage().getParameters().get('rec');
        if(recType==RecType1){
            showFirst=true;
        }
        if(recType==RecType2){
            showSecond=true;
        }
    
    
    }
    
    public PageReference saveit(){
        system.debug('CampData--->'+camp );
        
        
        return null;
    }

}