@IsTest
public class AccountManagerTest {
    
    @isTest static void testGetAccountById(){
         Id recordId = createTestRecord();
		 RestRequest request = new RestRequest();
        request.requestUri = 'https://ap2.salesforce.com/services/apexrest/Accounts/'+recordId+'/contacts';
        request.httpMethod = 'GET';
		RestContext.request = request;
		Account acc = AccountManager.getAccount();
        
    }
	
    
     static Id createTestRecord() {
        // Create test record
        Account accTest = new Account(Name='Test record');
        insert accTest;
         Contact con = new Contact();
         con.lastName = 'Test Contact';
         con.AccountId=accTest.Id;
        return accTest.Id;
    }  
}