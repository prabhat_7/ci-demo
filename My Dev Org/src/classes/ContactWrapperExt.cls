global class ContactWrapperExt{

    public list<Account> accList ;
    public String retURL {get;set;}
    public list<ContactWrapper> cwList {get;set;}
    global list<newOpps> processNewopps {get;set;}
    public Integer counter {get;set;}
    public map<Integer,Integer> params {get;set;}
    public Boolean closeWindow {get;set;}
    list<Contact> contList ;
    public String selected {get;set;}
    public set<String> selectedIds {get;set;}
    Static String popup ;
    public string pgv;
    global ContactWrapperExt() {
        cwList = new list<ContactWrapper>();
        processNewopps = new list<newOpps>();
        retURL = ApexPages.currentPage().getParameters().get('id');
        pgv = ApexPages.currentPage().getParameters().get('type');
        counter = 0;
        popup='';
        contList = new list<Contact>();
        closeWindow=false;
        selectedIds=new set<String>();
        params = new map<Integer,Integer>();
        
        if(retURL!=null && retURL.length()>0){
            contList = [SELECT Id,Name, Phone,LeadSource,Start_Date__c,Account.Name FROM Contact WHERE AccountId=:retURL];
            
            if(!contList.isEmpty()){
                Integer i=1;
                for(Contact cont : contList){
                    ContactWrapper cw = new ContactWrapper();
                    cw.con=cont;
                    cw.srNo = i;
                    cwList.add(cw);
                    i++;
                }
            }
        }
        
        system.debug('pgv --->'+pgv ); 
        if(pgv !=null && pgv.length()>0 && pgv=='risk'){
            captureValue();
        }
        system.debug('wrapper--->'+cwList);
    }

    public void fetchValues(){
       /*
        list<Account> acclist = new list<Account>([SELECT Id,Hidden_Values__c FROM Account WHERE Id=:retURL]);
        accList[0].Hidden_Values__c='dummy';
        for(String s : selectedIds){
            accList[0].Hidden_Values__c +=','+s;
        }
        update acclist; */
         system.debug('Hellloooooooo--->'+selectedIds);
        // system.debug('Hellaaaaccccco--->'+accList);
    }
    
    public void captureValue(){
       
        if(selected!=null && selected.length()>0){
            selectedIds.add(selected); 
        }
         system.debug('popup--->'+popup);
         system.debug('selected--->'+selectedIds);
    }
    
   
    
    public void processAllrisks(set<String> strIds){
        system.debug('selectedIds--->'+strIds);
        if(strIds!=null && strIds.size()>0){
            Integer intr =1;
            for(String str : strIds){
                newOpps nw = new newOpps();
                nw.sNo = intr;
                nw.Opp.Contact_ID__c=str;
                processNewopps.add(nw);
                intr++;
            }
        }
        system.debug('processNewopps--->'+processNewopps);
    }
    
    //Save Method
    public pageReference saveCons(){
        system.debug('wrap--->'+cwList);
        for(ContactWrapper cwr : cwList){
            if(cwr.con.LeadSource==null || cwr.con.Phone==null){
                if(cwr.con.LeadSource==null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Lead Source: value required.'));
                    cwr.con.LeadSource.addError('Value required');
                }
                if(cwr.con.Phone==null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Phone: value required.'));
                    cwr.con.Phone.addError('Value required');
                }
                return null;
            }
        }
        system.debug('wrap--->'+cwList);
        List<Contact> conList = new list<Contact>();
        for(ContactWrapper cw : cwList){
                cw.con.AccountId=accList[0].Id;
                cw.con.LastName = 'Contact '+cw.con.LeadSource;
                conList.add(cw.con);
         }
        if(!conList.isEmpty()){
            insert conList;
            closewindow = true;
        }
    
        return null;
    }
    
    public void populateDetail(){
    
       
    }
    
    
    // WrapperClass
    
    public class ContactWrapper {
        public Contact con {get;set;}
        public String paramLabel {get;set;}
        public String paramvalue {get;set;}
        public integer srno {get;set;}
        public Boolean check {get;set;}
        
        public ContactWrapper(){
            check = false;
            con = new Contact();
        
        }
    }
    
    global class newOpps {
        public Opportunity opp {get;set;}
        public integer sNo {get;set;}
        public newOpps(){
            opp = new Opportunity();
        
        }
    }

}