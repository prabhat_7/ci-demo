@isTest
public class DailyLeadProcessorTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static TestMethod void testSchedule(){
        list<Lead> leadlst = new list<Lead>();
        
        for(Integer i=0;i<200;i++){
            Lead l = new lead();
            l.LastName = 'test lead '+i;
            l.Company='Test Comp '+i;
            l.Status='Working-Contacted';
            leadlst.add(l);
        }
        insert leadlst;
        
         Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new DailyLeadProcessor()); 
        test.stopTest();
        
    }

}