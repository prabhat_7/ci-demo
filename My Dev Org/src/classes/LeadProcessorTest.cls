@isTest
public class LeadProcessorTest {
    
    static testMethod void testBatches(){
        list<Lead> allLeads = new list<Lead>();
        
        for(Integer i=0;i<200;i++){
            lead l = new lead();
            l.LastName='TestLead '+i;
            l.Company='TestCompany '+i ;
            l.LeadSource ='Web';
            l.Status='Working-Contacted';
            allLeads.add(l);
            
        }
        
        insert allLeads;
        
        Test.startTest();
        LeadProcessor lp = new LeadProcessor();
        Id batchId = database.executeBatch(lp);
        Test.stopTest();
        
    }

}