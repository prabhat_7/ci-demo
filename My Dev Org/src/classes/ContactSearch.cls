public class ContactSearch {

    public static list<Contact> searchForContacts(String s1,String s2){
        
        list<Contact> results = [SELECT Id,Name FROM Contact WHERE lastName=:s1 AND MailingPostalCode =:s2];
        
        return results;
        
    }
    
}